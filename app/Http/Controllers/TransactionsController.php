<?php

namespace App\Http\Controllers;

use App\Transactions;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
        {
            return datatables()->of(Transactions::latest()->get())
                    ->addColumn('action', function($data){
                        if($data->status == 'Pending')
                        {
                            $button = '<a class="btn btn-primary btn-sm" href="/transactions/'.$data->id.'/edit" name="edit"><i class="fas fa-edit"></i></a>';
                            $button .= '&nbsp;&nbsp;';
                            $button .= '<a class="btn btn-danger btn-sm" href="/transactions/delete/'.$data->id.'" name="delete"><i class="fas fa-trash"></i></a>';
                        }
                        else
                            $button = '<a class="btn btn-danger btn-sm" href="/transactions/delete/'.$data->id.'" name="delete"><i class="fas fa-trash"></i></a>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('transactions.read');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'tx_amount'=>'required|numeric'
        ]); 

        $transaction = new Transactions;
        $tx_amount = $request->input('tx_amount');
        if ($tx_amount <= 1000000)
        {
            $transaction->division = 'Finance';
            $transaction->status = 'Approved';
        }
        elseif ($tx_amount > 1000000 && $tx_amount <= 5000000)
        {
            $transaction->division = 'Direktur Keuangan';
            $transaction->status = 'Pending';
        }
        else
        {
            $transaction->division = 'Direktur Utama';
            $transaction->status = 'Pending';
        }
        $transaction->tx_amount = $request->input('tx_amount');    
        
        $transaction->save();

        return redirect('/transactions')->with('success','New transaction has been successfully inputted');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transactions $transactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transaction = Transactions::find($id);
        return view('transactions.update', ['transaction' => $transaction]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'tx_amount'=>'required|numeric',
            'status'=>'required'
        ]);

        $status = $request->input('status');
        $tx_amount = $request->input('tx_amount');

        $transaction = Transactions::find($id);
        $transaction->tx_amount = $tx_amount;    
        $transaction->status = $status;
        $transaction->save();

        return redirect('/transactions')->with('success','Transaction has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transactions::find($id);
        $transaction->delete();

        return redirect('/transactions')->with('alert','Deleted Transaction');
    }
}
