@extends('adminlte::page')

@section('title', 'LATIHAN1')

@section('content_header')
    <h1>
        Transactions
        <small>Latihan1</small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active">Transactions</li>
    </ol>
@stop

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>
    @endif
    @if(session()->has('alert'))
        <div class="alert alert-danger">
            {{ session()->get('alert') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>
    @endif
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Transactions Request</h3>
        </div>
        <div class="box-body">
            <a href="/transactions/create" class="btn btn-primary">Input New Request</a>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped" id="transact_table" >
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Amount (Rp.)</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop

@section('adminlte_js')
    <script>
        $(document).ready(function(){
            $('#transact_table').DataTable({
            searching: false,
            processing: true,
            serverSide: true,
            aaSorting: [[2,'desc']],
            ajax:{
            url: "{{ route('transactions.index') }}",
            },
            columns:[
            // {
            //     data: 'image',
            //     name: 'image',
            //     render: function(data, type, full, meta){
            //     return "<img src={{ URL::to('/') }}/images/" + data + " width='70' class='img-thumbnail' />";
            //     },
            //     orderable: false
            // },
            {
                data: 'id',
                render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                },
                orderable: false
            },
            {
                data: 'tx_amount',
                name: 'tx_amount',
                render: function(data,type,row){
                    return 'Rp. ' + row['tx_amount'];
                },
                orderable: false
            },
            {
                data: 'created_at',
                name: 'created_at',
                orderable: false
            },
            {
                data: 'status',
                name: 'status',
                render: function(data,type,row){
                    var r = '';
                    if(row['tx_amount'] > 1000000 && row['tx_amount'] <= 5000000 && row['status'] == 'Pending')
                        r = 'Pending (Need Direktur Keuangan Approval)';
                    else if(row['tx_amount'] > 5000000 && row['status'] == 'Pending')
                        r = 'Pending (Need Direktur Utama Approval)';
                    else if (row['status']=='Declined')
                        r = 'Declined';
                    else if (row['status'] == 'Approved')
                        r = 'Approved';
                    else
                        r = 'Pending Approval';
                    return r;
                },
                orderable: false
            },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
            ]
            });
        });
    </script>
@stop