@extends('adminlte::page')

@section('title', 'LATIHAN1')

@section('content_header')
    <h1>
        Transactions Request
        <small>Latihan1</small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Dashboard</a></li>
        <li><a href="/transactions">Transactions</a></li>
        <li class="active">Edit Request</li>
    </ol>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Request</h3>
        </div>
        <div class="box-body">
        <form action="/transactions/{{$transaction->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Transaction Amount (Rp.)</label>
                <input type="number" name="tx_amount" class="form-control" placeholder="Rp. 123456" value="{{ $transaction->tx_amount }}">

                    @if($errors->has('tx_amount'))
                        <div class="text-danger">
                            {{ $errors->first('tx_amount')}}
                        </div>
                    @endif
                <br>
                <div class="form-group">
                    <label>Set Status</label>
                    <br>
                    <select class="custom-select" name="status">
                        <option value="Approved" selected>Approve</option>
                        <option value="Declined">Decline</option>
                      </select>
                      @if($errors->has('status'))
                      <div class="text-danger">
                          {{ $errors->first('status')}}
                      </div>
                  @endif  
                </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Update">
                </div>
            </form>
        </div>
    </div>
@stop