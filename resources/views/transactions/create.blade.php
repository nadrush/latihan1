@extends('adminlte::page')

@section('title', 'LATIHAN1')

@section('content_header')
    <h1>
        Transactions Request
        <small>Latihan1</small>
    </h1>

    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Dashboard</a></li>
        <li><a href="/transactions">Transactions</a></li>
        <li class="active">New Request</li>
    </ol>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Input New Request</h3>
        </div>
        <div class="box-body">
            <form action="{{ route('transactions.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label>Transaction Amount (Rp.)</label>
                <input type="number" name="tx_amount" class="form-control" placeholder="Rp. 123456" value="{{ old('tx_amount') }}">

                    @if($errors->has('tx_amount'))
                        <div class="text-danger">
                            {{ $errors->first('tx_amount')}}
                        </div>
                    @endif

                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Input">
                </div>
            </form>
        </div>
    </div>
@stop